package com.example.mapper.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.example.constants.Constants;
import com.example.domain.User;
import com.example.dto.UserDto;
import com.example.mapper.UserMapper;
import com.example.service.RoleService;

public class UserMapperDecorator implements UserMapper {

	@Autowired
	@Qualifier("delegate")
	private UserMapper delegate;

	@Autowired
	private RoleService roleService;

	@Override
	public User userDtoToUser(UserDto userDto) {
		User user = delegate.userDtoToUser(userDto);
		populateUserObject(user, userDto);
		return user;
	}

	@Override
	public UserDto userToUserDto(User user) {
		UserDto userDto = delegate.userToUserDto(user);
		List<Long> roleIds = new ArrayList<Long>();
		List<String> roleNames = new ArrayList<String>();
		user.getUserRoles().forEach(userRoles -> {
			roleIds.add(userRoles.getId());
			roleNames.add(userRoles.getName());
		});
		userDto.setRoleIds(roleIds);
		userDto.setRoleNames(roleNames);

		return userDto;
	}

	private void populateUserObject(User user, UserDto userDto) {
		if (userDto.getRoleIds().size() > Constants.ZERO_INTEGER) {
			addUserRole(user, userDto);
		}
	}

	private void addUserRole(User user, UserDto userDto) {
		user.setUserRoles(roleService.findRoleByIds(userDto.getRoleIds()));
	}

}
