package com.example.mapper;

import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import com.example.domain.User;
import com.example.dto.UserDto;
import com.example.mapper.impl.UserMapperDecorator;

@Mapper(componentModel = "spring", uses = {})
@DecoratedWith(UserMapperDecorator.class)
public interface UserMapper {

	@Mappings({})
	public User userDtoToUser(UserDto userDto);

	@Mappings({ @Mapping(target = "password", ignore = true), })
	public UserDto userToUserDto(User user);

}
