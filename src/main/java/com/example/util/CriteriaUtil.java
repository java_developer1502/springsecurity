package com.example.util;

import org.hibernate.Criteria;

public class CriteriaUtil {

	public static void setCriteriaPageData(Criteria criteria, int pageNumber,
			int maxPageLimit) {
		int startIndex = (pageNumber - 1) * maxPageLimit;
		criteria.setFirstResult(startIndex);
		criteria.setMaxResults(maxPageLimit);
	}

	/*public static void setCriteriaOrder(Criteria criteria, String fieldName) {
		if (fieldName.startsWith("-")) 
			fieldName = fieldName.subString(0);
			criteria.addOrder(Order.desc(fieldName));
		} else {
			criteria.addOrder(Order.asc(fieldName));
		}
	}*/
}
