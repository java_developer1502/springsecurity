package com.example.constants;

public class Constants {

	public static final String LIQUIBASE_XML_FILE_NAME = "liquibase/liquibase-changeLog.xml";
	public static final String CLASSPATH = "classpath";
	public static final int DEFAULT_PAGE_SIZE = 10;
	public static final String PLUS_SYMBOL = "+";
	public static final String MINUS_SYMBOL = "-";
	public static final Integer ZERO_INTEGER=0;
	public static final String ENTITY_ID = "id";
	

}
