package com.example.dao;

import java.util.List;

import com.example.domain.UserRole;


public interface RoleRepository {
	
	List<UserRole> findRoleByIds(List<Long> ids);
	
	UserRole findRoleById(Long id);
	
	List<UserRole> findRolesByUserId(Long id);

}
