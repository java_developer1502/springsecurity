package com.example.dao;

import java.util.List;

import com.example.domain.User;


public interface UserRepository{
	public User findByUserName(String username);
	
	public void saveUser(User user);
	
	public User findById(Long id);
	
	public Long getRowCount();
	
	public List<User> findUsersByPageNumber(Integer pageNumber);
	
}
