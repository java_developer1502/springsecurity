package com.example.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.example.domain.UserRole;

@Repository
public class RoleRepositoryImpl extends AbstractDao<UserRole> implements RoleRepository{

	@Override
	public List<UserRole> findRoleByIds(List<Long> ids) {
		Criteria criteria = createEntityCriteria();
		criteria = criteria.add(Restrictions.in("id", ids));
		return criteria.list();
	}

	@Override
	public UserRole findRoleById(Long id) {
		return getByPrimaryKey(id);
	}

	@Override
	public List<UserRole> findRolesByUserId(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

}
