package com.example.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.example.constants.Constants;
import com.example.domain.User;
import com.example.util.CriteriaUtil;

@Repository
public class UserRepositoryImpl extends AbstractDao<User> implements UserRepository{

	@Autowired
	SessionFactory sessionFactory;
	
	public User findByUserName(String username){
		 Criteria crit = createEntityCriteria();
	        crit.add(Restrictions.eq("username", username));
	        User user = (User)crit.uniqueResult();
	        return user;		
	}

	@Override
	public void saveUser(User user) {
		 save(user);
	}

	@Override
	public User findById(Long id) {
		return getByPrimaryKey(id);
	}

	@Override
	public Long getRowCount() {
		Criteria criteria = createEntityCriteria().setProjection(Projections.rowCount());
		Long count = (Long)criteria.uniqueResult();
		return count;
	}

	@Override
	public List<User> findUsersByPageNumber(Integer pageNumber) {
     Criteria criteria = createEntityCriteria();
     CriteriaUtil.setCriteriaPageData(criteria, pageNumber, Constants.DEFAULT_PAGE_SIZE);
     return criteria.list();
	}
	
}
