package com.example.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.dao.UserRepository;
import com.example.domain.User;
import com.example.service.RoleService;
import com.example.service.UserService;
import com.example.util.SecurityUtil;
 
 
@Service("userService")
@Transactional
public class UserServiceImpl implements UserService{
 
    @Autowired
    private UserRepository userRepository;
   
    
    @Autowired
    private RoleService roleService;

    @Override
    public User findByUserName(String username) {
        User user = userRepository.findByUserName(username);
        return user;
    }

	@Override
	public void saveUser(User user) {
		userRepository.saveUser(user);
	}

	@Override
	public User findById(Long id) {
		return userRepository.findById(id);
	}

	@Override
	public User getAuthenticatedUser() {
		User user = findByUserName(SecurityUtil.getAuthenticatedUser());
		return	user; 
	}

}