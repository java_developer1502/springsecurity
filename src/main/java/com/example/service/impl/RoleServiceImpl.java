package com.example.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.dao.RoleRepository;
import com.example.domain.UserRole;
import com.example.service.RoleService;

@Service
@Transactional
public class RoleServiceImpl implements RoleService{
	
	@Autowired
	RoleRepository roleRepository;

	@Override
	public List<UserRole> findRoleByIds(List<Long> ids) {
		return roleRepository.findRoleByIds(ids);
	}

	@Override
	public UserRole findRoleById(Long id) {
		return roleRepository.findRoleById(id);
	}

	@Override
	public List<UserRole> findRolesByUserId(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

}
