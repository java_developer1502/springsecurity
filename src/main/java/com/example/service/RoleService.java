package com.example.service;

import java.util.List;

import com.example.domain.UserRole;

public interface RoleService {

	List<UserRole> findRoleByIds(List<Long> ids);

	UserRole findRoleById(Long id);
	
	List<UserRole> findRolesByUserId(Long id);

}
