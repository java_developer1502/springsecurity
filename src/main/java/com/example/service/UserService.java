package com.example.service;

import com.example.domain.User;
 
 
public interface UserService {
     
	
	User findById(Long id);
     
    User findByUserName(String username);
    
    void saveUser(User user);
    
    User getAuthenticatedUser();
    
     
    
 
}