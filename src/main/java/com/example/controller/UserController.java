package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.domain.User;
import com.example.dto.UserDto;
import com.example.mapper.UserMapper;
import com.example.service.UserService;

@RestController
@RequestMapping("/api/users")
public class UserController {

	private final UserService userService;
	
	 @Autowired
	 UserMapper userMapper;

	@Autowired
	UserController(UserService userService) {
		this.userService = userService;
	}	
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public void saveUser(@RequestBody UserDto userDto) {
		User user = userMapper.userDtoToUser(userDto);
		userService.saveUser(user);
	}

	@RequestMapping(value = "/id/{id}", method = RequestMethod.GET)
	public ResponseEntity<User> userById(@PathVariable Long id) {
		User user = userService.findById(id);
		return new ResponseEntity<User>(user, HttpStatus.OK);
	}

	@RequestMapping(value = "/check", method = RequestMethod.POST)
	public void check(@RequestBody UserDto userDto) {
		User user = userMapper.userDtoToUser(userDto);
		System.out.println(user.getFirstName());
	}
	
	@RequestMapping(value = "/me", method = RequestMethod.GET)
	public ResponseEntity<UserDto> findMe() {
		User user = userService.getAuthenticatedUser();
		UserDto userdto = userMapper.userToUserDto(user);
		return new ResponseEntity<UserDto>(userdto, HttpStatus.OK);
	}
	
	
	
	
}
