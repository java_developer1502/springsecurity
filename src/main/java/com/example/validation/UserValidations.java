package com.example.validation;

import java.util.Optional;

import org.springframework.stereotype.Component;

import com.example.constants.Constants;


@Component
public class UserValidations {

	public Integer getDefaultPageNumber(Optional<Integer> pageNumber){
		if(pageNumber.isPresent()){
			return pageNumber.get();
		}else{
			return Constants.ZERO_INTEGER;
		}
	}
	
	
}
